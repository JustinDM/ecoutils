using System;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Extensions;
using Economical.EcoObjects.General.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Economical.EcoUtils.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConsoleController : ControllerBase
    {
        [HttpGet("EmailLookup/{characterId}")]
        public async Task<IActionResult> LookupEmailAddress(string characterId)
        {
            if (characterId.Length != 3)
            {
                return BadRequest("character ID must be 3 characters long");
            }

            var command = $"dsquery user -samid \"{characterId}\" | dsget user -email";

            var commandOutput = EcoConsole.CallConsoleCommand(command);

            var emails = commandOutput.GetMatches(@"([a-zA-Z]|[0-9]|-|\.)+@[a-zA-Z]+\.com");


            return Ok(emails);
        }

        [HttpGet("ManagerEmailLookup/{characterId}")]
        public async Task<IActionResult> LookupManagerEmail(string characterId)
        {
            if (characterId.Length != 3)
            {
                return BadRequest("character ID must be 3 characters long");
            }

            var managerCommand = $"dsquery user -samid \"{characterId}\" | dsget user -mgr";

            var managerCommOut = EcoConsole.CallConsoleCommand(managerCommand);

            var managers = managerCommOut.GetMatches("CN=([a-zA-Z]|\\s)+");

            var emails = await LookupEmailByName(managers[0].ToString().Replace("CN=", ""));

            return emails;
        }

        [HttpGet("NameEmailLookup/{userName}")]
        public async Task<IActionResult> LookupEmailByName(string userName) 
        {
            if (userName.Contains(';') || userName.Contains("|") || userName.Contains("...") || userName.Contains("&&"))
            {
                return BadRequest("name contains invalid characters");
            }

            if (userName.Contains("("))
            {
                userName = userName.Substring(0, userName.IndexOf("("));
            }

            if (userName.Contains(", "))
            {
                var userNameItems = userName.Split(new string[] {", "}, StringSplitOptions.RemoveEmptyEntries);

                userName = userNameItems[1].Trim() + " " + userNameItems[0].Trim();
            }

            var command = $"dsquery user -name \"{userName}\" | dsget user -email";

            var commandOutput = EcoConsole.CallConsoleCommand(command);

            var emails = commandOutput.GetMatches(@"([a-zA-Z]|[0-9]|-|\.)+@[a-zA-Z]+\.com");

            return Ok(emails);
        }
    }
}