using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.EcoUtils;
using Economical.EcoObjects.General.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Economical.EcoUtils.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        [HttpGet("charid/{charId}")]
        public async Task<IActionResult> GetUsersByChar(string charId)
        {
            if (charId.Length > 3)
            {
                return BadRequest("char id must be 3 characters long");
            }

            if (charId.Length < 3 && charId.Contains("*") == false)
            {
                return BadRequest("char id can only be less than 3 characters if it contains the '*' wildcard");
            }

            var users = new List<User>();

            var command = $"dsquery user -samid \"{charId}\"";

            var commandOutput = EcoConsole.CallConsoleCommand(command);
            
            var outputLines = commandOutput.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var i in outputLines)
            {
                var user = new User(i);
                users.Add(user);
            }

            return Ok(users);
        }

        [HttpGet("name/{name}")]
        public async Task<IActionResult> GetUsersByName(string name)
        {
            if (name.Contains(';') || name.Contains("|") || name.Contains("...") || name.Contains("&&"))
            {
                return BadRequest("name contains invalid characters");
            }

            if (name.Contains(","))
            {
                var nameItems = name.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                name = $"{nameItems[1].Trim()} {nameItems[0].Trim()}";
            }

            var command = $"dsquery user -name \"{name}\"";

            var users = new List<User>();

            var commandOutput = EcoConsole.CallConsoleCommand(command);
            
            var outputLines = commandOutput.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var i in outputLines)
            {
                var user = new User(i);
                users.Add(user);
            }

            return Ok(users);
        }

        [HttpPost("manager")]
        public async Task<IActionResult> GetUserManager([FromBody] User user)
        {
            // dsquery * "CN=Dhanvin Raval,OU=WAT1,OU=EIGUsers,DC=eig,DC=ecogrp,DC=ca" -attr mail

            var command = $"dsquery * \"{user.DistinguishedName}\" -attr manager";

            var output = EcoConsole.CallConsoleCommand(command);

            var outputLines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            outputLines = outputLines.Skip(1).ToArray();
            var joinedOutput = String.Join("", outputLines);

            var outputItems = joinedOutput.Split(";");

            if (outputItems.Length > 1)
            {
                return BadRequest("More than one manager for user");
            }

            foreach (var i in outputItems)
            {
                var manager = new User(i.Trim());
                user.Manager = manager;
            }

            return Ok(user);
        }

        [HttpPost("directreports")]
        public async Task<IActionResult> GetUserDirectReports([FromBody] User user)
        {
            var command = $"dsquery * \"{user.DistinguishedName}\" -attr DirectReports";

            var output = EcoConsole.CallConsoleCommand(command);

            var outputLines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            outputLines = outputLines.Skip(1).ToArray();
            var joinedOutput = String.Join("", outputLines);

            var outputItems = joinedOutput.Split(";");

            foreach (var i in outputItems)
            {
                var report = new User(i);
                user.DirectReports.Add(report);
            }

            return Ok(user);
        }

        [HttpPost("email")]
        public async Task<IActionResult> GetUserEmail([FromBody] User user)
        {
            var command = $"dsquery * \"{user.DistinguishedName}\" -attr mail";

            var output = EcoConsole.CallConsoleCommand(command);

            var outputLines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            outputLines = outputLines.Skip(1).Where(x => string.IsNullOrWhiteSpace(x) == false).ToArray();
            var joinedOutput = String.Join("", outputLines);

            if (outputLines.Length > 2)
            {
                return BadRequest("Multiple emails returned by command");
            }

            var outputItems = joinedOutput.Split(";");

            foreach (var i in outputItems)
            {
                user.Email = i.Trim();
            }

            return Ok(user);
        }
        [HttpPost("charid")]
        public async Task<IActionResult> GetUserCharacterId([FromBody] User user)
        {
            var command = $"dsquery * \"{user.DistinguishedName}\" -attr SamAccountName";

            var output = EcoConsole.CallConsoleCommand(command);

            var outputLines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            outputLines = outputLines.Skip(1).Where(x => string.IsNullOrWhiteSpace(x) == false).ToArray();
            var joinedOutput = String.Join("", outputLines);

            if (outputLines.Length > 2)
            {
                return BadRequest("Multiple three character IDs returned by command");
            }

            var outputItems = joinedOutput.Split(";");

            foreach (var i in outputItems)
            {
                user.SamAccountName = i.Trim();
            }

            return Ok(user);
        }
    }
}